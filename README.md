# ![MakeMake logo](https://framagit.org/makemake/makemake/-/raw/main/makemake-logo.png) MakeMake
A dwarf planet for your feeds.

v1.0

## License

This software is released under [AGPLv3+ license](https://www.gnu.org/licenses/agpl-3.0.html)


## Installing

    pip install -r requirements.txt


## Specifying sources

Simply edit a sources.yml file in _datas_ folder:

    -
      name: "MakeMake commit history"
      url: "https://framagit.org/makemake/makemake/-/commits/main?format=atom"
      link: "https://framagit.org/makemake/makemake"
      avatar: "https://framagit.org/makemake/makemake/-/raw/main/makemake-logo.png"

Thus, you can filter a feed if necessary (see [datas/sources.default.yml](https://framagit.org/makemake/makemake/-/blob/main/datas/sources.default.yml)):

    -
      name: "MakeMake commit history"
      url: "https://framagit.org/makemake/makemake/-/commits/main?format=atom"
      link: "https://framagit.org/makemake/makemake"
      avatar: "https://framagit.org/makemake/makemake/-/raw/main/makemake-logo.png"
      filters:
        -
          content: "feat:"

This filter will create new entry only for _feature_ commits for example (as soon as
commit messages are well formed of course).

**Note:** Since update will only perform actions on new entries, filters will be used
only on new entries. Old entries not filtered before will still exists.


## Serving dynamic content

Set your environment config

    cp default.env .env

and create some environment variables if necessary.

Update data once

    python3 update.py

And start application

    gunicorn --workers 2 --bind 0.0.0.0:8000 makemake


## Serving static content

Set your environment config

    cp default.env .env

and create some environment variables if necessary.

Update data saying which folder will receive static content

    python3 update.py --static /www/planet

and open a web server to target your static folder


## Theming

In order to change the display of your Planet, you can use some existing theme or make
your own. Existing themes have their own repo:
https://framagit.org/makemake/makemake-themes.


## Changelog

### v1.0

* Upgrade to Flask 3.1.0
* Use .env files or environment variables for specific configuration


## Contributors

* Mindiell - main developper (mastodon: https://mamot.fr/@Mindiell)
* Péhä - logo designer (mastodon: https://framapiaf.org/@peha)
